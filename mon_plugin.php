<?php
/**
 * Plugin Name:  Mon Plugin
 * Description:  Mon premier plugin pour apprendre
 * Version:      0.1.0
 * Author:       Maxime Montagne
 * Requires PHP: 8.0
 **/
require 'vendor/autoload.php';

use MonPlugin\PostTypes\Hero;

Hero::register();

add_filter('the_content', 'censure');

function censure($content)
{
    $blackList = explode(' ', get_option('censure_blacklist'));
    return str_ireplace($blackList, '*****', $content);
}

add_shortcode('coucou', 'coucou_cb');

function coucou_cb()
{
    return 'coucou depuis un shortcode';
}

// Actions pour le menu Censure
add_action('admin_menu', 'ajout_menu_censure');
add_action('admin_init', 'register_censure_settings');

function ajout_menu_censure()
{
    add_options_page(
        "Gestion de la censure",
        "Censure",
        "manage_options", // Capabilities (les permissions)
        "menu_censure",
        'render_menu_censure' // Function d'affichage de la page
    );
}

function render_menu_censure()
{
    // Je ferme PHP pour écrire mon HTML
    ?>
    <h1>Gestion de la censure des Posts</h1>
    <form action="options.php" method="post">
        <?php
        settings_fields('censure_options');
        do_settings_sections('censure_options');
        submit_button();
        ?>
    </form>
    <?php
    // Je n'oublis pas de rouvrir PHP !!!
}

function register_censure_settings()
{
    register_setting(
        'censure_options', // Nom de notre Groupe
        'censure_blacklist', // Le nom de l'option que l'on veut enregistrer
        [
            'default' => '', // On peut passer une valeur par défaut
        ]);


    // Création d'une section du formulaire
    add_settings_section(
        'censure_options_section1', // Nom de la section
        'Section 1 de la censure', // Titre
        function () { // Callback qui doit afficher quelque chose
            echo 'Petit message de présentation de la section';
        },
        'censure_options' // nom du Group
    );

    // Création d'un champs du formulaire
    add_settings_field(
        'censure_blacklist',
        'Liste des mots interdits',
        function () {
            ?>
            <textarea name="censure_blacklist" cols="30" rows="10"><?= get_option('censure_blacklist') ?></textarea>
            <?php
        },
        'censure_options',
        'censure_options_section1',
    );

    register_setting(
        'censure_options', // Nom de notre Groupe
        'censure_coucou', // Le nom de l'option que l'on veut enregistrer
        [
            'default' => 'plop', // On peut passer une valeur par défaut
        ]);

    // Création d'un champs du formulaire
    add_settings_field(
        'censure_coucou',
        'Liste des mots coucou',
        function () {
            $choices = ['easy', 'medium', 'hard', 'coucou', 'plop'];
            $current = get_option('censure_coucou');
            ?>
            <select name="censure_coucou">
                <?php foreach($choices as $choice): ?>
                    <option value="<?=$choice?>" <?= $choice === $current ? 'selected' : ''  ?> ><?= $choice ?></option>
                <?php endforeach ?>
            </select>
            <?php
        },
        'censure_options',
        'censure_options_section1',
    );
}














