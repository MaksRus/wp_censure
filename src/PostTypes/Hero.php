<?php

namespace MonPlugin\PostTypes;

class Hero
{
    const KEY = 'hero';

    public static function register()
    {
        add_action('init', [self::class, 'monplugin_register_hero']);
    }

    public static function monplugin_register_hero()
    {
        register_post_type(self::KEY, [
            'label' => 'Héros',
            'description' => 'Des héros de rpg',
            'public' => true,
            'show_in_menu' => true,
            'show_in_rest' => true,
            'menu_position' => 4,
            'menu_icon' => 'dashicons-shield',
            'supports' => [
                'title',
                'editor',
                'thumbnail',
                'comments',
            ],
            "taxonomies" => [],
            'has_archive' => true,
        ]);
    }

}
